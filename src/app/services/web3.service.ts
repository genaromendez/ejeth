import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { bindNodeCallback, Observable } from 'rxjs';
import { AbiItem } from 'web3-utils';
import { Buffer } from 'buffer';
import { Subject } from 'rxjs';

import Web3 from 'web3';
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";

@Injectable({providedIn: 'root'})
export class Web3Service {
    public abi: any;
    public contractAddress: string;
    public contrato: any;
    public accounts: any;
    public web3js: any;    
    public provider: any; 
    public conectado: boolean = false;
    public msgEstado: string = "No conectado";

    public explorerTx: string = "ropsten.etherscan.io/tx/"
    public explorerBl: string = "ropsten.etherscan.io/block/"
    public explorerAd: string = "ropsten.etherscan.io/address/"
    
    web3Modal;
    
    private accountStatusSource = new Subject<any>();
    accountStatus$ = this.accountStatusSource.asObservable();

    constructor() {
        const providerOptions = {
            walletconnect: {
              package: WalletConnectProvider, // required
              options: {
                infuraId: "wss://ropsten.infura.io/ws/c0c8c037208043debd3192efe93ed1d2" // required
              }
            }
          };

        this.web3Modal = new Web3Modal({
            network: "ropsten", // optional
            cacheProvider: true, // optional
            providerOptions, // required
            theme: {
                background: "rgb(39, 49, 56)",
                main: "rgb(199, 199, 199)",
                secondary: "rgb(136, 136, 136)",
                border: "rgba(195, 195, 195, 0.14)",
                hover: "rgb(16, 26, 32)"
            }
        });

        // tslint:disable-next-line:max-line-length                      
		this.abi = JSON.parse('[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"solicitante","type":"address"},{"indexed":false,"internalType":"uint256","name":"nofactura","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"fecha","type":"uint256"},{"indexed":false,"internalType":"string","name":"strMensaje","type":"string"}],"name":"EventConsultaRecinto","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"solicitante","type":"address"},{"indexed":false,"internalType":"uint256","name":"nofactura","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"fecha","type":"uint256"},{"indexed":false,"internalType":"bytes32","name":"rfc","type":"bytes32"},{"indexed":false,"internalType":"string","name":"url","type":"string"},{"indexed":false,"internalType":"string","name":"strMensaje","type":"string"}],"name":"EventRegistroFactura","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"string","name":"message","type":"string"}],"name":"ThrowError","type":"event"},{"inputs":[{"internalType":"address","name":"_paddReceptor","type":"address"},{"internalType":"uint256","name":"_pfactura","type":"uint256"},{"internalType":"uint256","name":"_pfecha","type":"uint256"},{"internalType":"bytes32","name":"_pRFC","type":"bytes32"},{"internalType":"bytes32","name":"_ptipomaniobra","type":"bytes32"},{"internalType":"string","name":"_strCliente","type":"string"},{"internalType":"string","name":"_strNomAgente","type":"string"},{"internalType":"string","name":"_strUrl","type":"string"}],"name":"RegistroFactura","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"stateMutability":"payable","type":"constructor"},{"inputs":[{"internalType":"uint256","name":"_valor","type":"uint256"}],"name":"ConsultaFactura","outputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bytes32","name":"","type":"bytes32"},{"internalType":"bytes32","name":"","type":"bytes32"},{"internalType":"string","name":"","type":"string"},{"internalType":"string","name":"","type":"string"},{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256[]","name":"_valor","type":"uint256[]"}],"name":"ConsultaFactura2","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"FacturasMuellaje","outputs":[{"internalType":"address","name":"addEmisor","type":"address"},{"internalType":"address","name":"addReceptor","type":"address"},{"internalType":"uint256","name":"intFolioSIP","type":"uint256"},{"internalType":"uint256","name":"intfecha","type":"uint256"},{"internalType":"bytes32","name":"byRFC","type":"bytes32"},{"internalType":"bytes32","name":"byTrafico","type":"bytes32"},{"internalType":"string","name":"strCliente","type":"string"},{"internalType":"string","name":"strNomAgente","type":"string"},{"internalType":"string","name":"strUrl","type":"string"}],"stateMutability":"view","type":"function"}]');
        // this.contractAddress = '0x97536a115c56B20a3F4642C05C788c0A526C5c99';
		this.contractAddress = '0x1B6ee88232E582C086150406A84647cB71711Fa2';
    }
      
    async connectAccount() {
        this.web3Modal.clearCachedProvider();
    
        this.provider = await this.web3Modal.connect(); // set provider
        this.web3js = new Web3(this.provider); // create web3 instance
        this.accounts = await this.web3js.eth.getAccounts(); 
        this.accountStatusSource.next(this.accounts)
        this.contrato = new this.web3js.eth.Contract(this.abi, this.contractAddress);   
    }  
    
}

/*

constructor() {
        this.web3 = new Web3();

        this.web3.setProvider(
            new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/v3/c0c8c037208043debd3192efe93ed1d2')
        );

        //this.sender = '0xf65112fa0998477c990fb71722b067b7892f2160';
        this.sender = '0xAFF7B3C82d2B3B2270db013E0Cbf2783ED514bF1';

        this.web3.eth.defaultAccount = this.sender;
        
        // tslint:disable-next-line:max-line-length              
        this.abi = JSON.parse('[{"constant":false,"inputs":[],"name":"increment","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"oldValue","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"newValue","type":"uint256"}],"name":"ValueChanged","type":"event"},{"constant":true,"inputs":[],"name":"getCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]');
        this.contractAddress = '0x99E5a41b66702D9d54428d48FE9C8dEE2DDc6CbC';

        this.contrato = new this.web3.eth.Contract(this.abi, this.contractAddress);
    }
*/
