import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import  {FormsModule} from "@angular/forms"
import { AppComponent } from './app.component';
import { Web3Service } from './services/web3.service';
import { RegistraFacturaComponent } from './registra-factura/registra-factura.component';
import { AppRoutingModule } from './app-routing.module';
import { ConsultarComponent } from './consultar/consultar.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistraFacturaComponent,
    ConsultarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [Web3Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
