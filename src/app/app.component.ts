import { Component, AfterViewInit, ElementRef, ViewChild,ViewChildren,QueryList} from '@angular/core';
import { Web3Service } from './services/web3.service';
import { Factura } from './models/Factura';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('scrollframe',{static:false}) private scrollFrame!: ElementRef;
  @ViewChildren('item') itemElements!: QueryList<any>;

  private scrollContainer: any;

  title = 'Ejemplo Ethereum';
   
  facturaConsultar = 0;
  fromBlock = 10900000;  

  factura: Factura = new Factura();

  blockHash = '';
  blockNumber = '';
  from = '';
  transactionHash = '';

  elementos: any = [];  

  cabeceras = ['Transaction Hash', 'Block Number','Solicitante', 'Factura', 'Fecha','RFC','Nombre','URL'];

  constructor(public web3s: Web3Service){}

  ngAfterViewInit(): void {
    this.conectar();
    //this.scrollContainer = this.scrollFrame.nativeElement;      
    //this.elementos.changes.subscribe(() => this.onElementosChanged());   
  }

    conectar():void {
    if (!this.web3s.conectado) {
      this.web3s.connectAccount().then((r)=>{ 
        this.web3s.msgEstado = "Conectado.";
        this.web3s.conectado = true;
        // this.subscribeToEvents();
      });
    }
  }
}
