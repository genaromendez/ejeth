import { Component, OnInit } from '@angular/core';
import { Web3Service } from '../services/web3.service';
import { Factura } from '../models/Factura';
import { createOfflineCompileUrlResolver } from '@angular/compiler';

@Component({
  selector: 'app-registra-factura',
  templateUrl: './registra-factura.component.html',
  styleUrls: ['./registra-factura.component.css'],
})
export class RegistraFacturaComponent implements OnInit {
  factura: Factura = new Factura();
  msgEstado = 'No Conectado.';
  estado = false;

  transactionHash = '';
  blockNumber = 0;
  status = true;
  proceso = -1; // -1 sin procesar, 0 procesando, 1 procesado, 2 error
  error = '';

  constructor(public web3s: Web3Service) {
    this.factura.emisor = '0x45B594c6986403Db885954aFf3A7D5656CE36DdE';
    this.factura.receptor = '0xAFF7B3C82d2B3B2270db013E0Cbf2783ED514bF1';
  }

  ngOnInit(): void {}

  conectar(): void {
    if (!this.web3s.conectado) {
      this.web3s.connectAccount().then((r) => {
        this.web3s.msgEstado = 'Conectado.';
        this.web3s.conectado = true;
        // this.subscribeToEvents();
      });
    }
  }

  registraFactura(): void {
    this.proceso = 0;
    this.error = '';
    let receptor: string = this.factura.receptor;
    let folioSIP: number = this.factura.folioSIP * 1;
    let fecha: number = new Date(this.factura.fecha).getTime();
    let rfc: string = this.web3s.web3js.utils.asciiToHex(this.factura.rfc);
    let byTrafico: string = this.web3s.web3js.utils.asciiToHex(
      this.factura.byTrafico
    );
    let strCliente: string = this.factura.strCliente;
    let strNomAgente: string = this.factura.strNomAgente;
    let strUrl: string = this.factura.strUrl;
    try {
      this.web3s.contrato.methods
        .RegistroFactura(
          receptor,
          folioSIP,
          fecha,
          rfc,
          byTrafico,
          strCliente,
          strNomAgente,
          strUrl
        )
        .send({ from: this.web3s.accounts[0] })
        .then((response: any) => {
          console.log(response);
          this.blockNumber = response.blockNumber;
          this.transactionHash = response.transactionHash;
          this.status = response.status;
          this.proceso = 1;
        })
        .catch((err: any) => {
          console.log(err);
          this.error = err;
          this.proceso = 2;
        });
    } catch (err) {
        this.error = JSON.stringify(err);
        this.proceso = 2;
        console.error(err);
    }
  }
}
