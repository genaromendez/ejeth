import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegistraFacturaComponent } from './registra-factura/registra-factura.component';
import { ConsultarComponent } from './consultar/consultar.component';

const routes: Routes = [  
  { path: '', component: ConsultarComponent },
  { path: 'registrar', component: RegistraFacturaComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
