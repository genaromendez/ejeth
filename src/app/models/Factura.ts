export class Factura {
    emisor:string = "";
    receptor:string = "";
    folioSIP: number = 0;
    fecha: Date = new Date();
    rfc:string = "";
    byTrafico:string = "";
    strCliente:string = "";
    strNomAgente:string = "";
    strUrl:string = "";

    clear(){
        this.emisor = "";
        this.receptor = "";
        this.folioSIP = 0;
        this.fecha = new Date();
        this.rfc = "";
        this.byTrafico = "";
        this.strCliente = "";
        this.strNomAgente = "";
        this.strUrl = "";
    }

    constructor(){};
    //constructor(emisor:string,receptor:string,folioSIP:number,fecha:Date,rfc:string,byTrafico:string,strCliente:string,strNomAgente:string,strUrl:string){};
}