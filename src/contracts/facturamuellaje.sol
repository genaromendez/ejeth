// SPDX-License-Identifier: MIT

pragma solidity ^0.6.4;

//import "./Funcionesfac.sol";

contract Facturamuellaje {
    // Funciones cF = new Funciones();

    //definir el owner
    address payable addowner;

    //Contador
    int256 contadorfacturas = 0;
    int256 contadordetalles = 0;
    int256 contadorsalidas = 0;

    //constructor de ejecución
    constructor() public payable {
        addowner = msg.sender;
        contadorfacturas = 0;
        contadordetalles = 0;
    }

    //structuras de almacenamiento
    struct EncabezadoFactura {
        address addEmisor;
        address addReceptor;
        uint256 intFolioSIP;
        uint256 intfecha;
        bytes32 byRFC;
        bytes32 byTrafico;
        string strCliente;
        string strNomAgente;
        string strUrl;
    }

    struct DetalleFactura {
        uint128 intFolioSIP;
    }

    struct SalidasFactura {
        uint256[2] _valores;
        /*intFolioSIP, fecha*/
        address addRegistro;
        string _strDescripcion;
    }

    //Mapeo de registros por ID de factura
    mapping(uint256 => EncabezadoFactura) public FacturasMuellaje;
    DetalleFactura[] detalleFactura;
    SalidasFactura[] salidasFactura;

    //Eventos que serán emitidos al realizar algún cambio
    event ThrowError(string message);
    event EventRegistroFactura(
        address solicitante,
        uint256 nofactura,
        uint256 fecha,
        bytes32 rfc,
        string url,
        string strMensaje
    );
    event EventConsultaRecinto(
        address solicitante,
        uint256 nofactura,
        uint256 fecha,
        string strMensaje
    );

    //modificadores
    modifier onlyOwner() {
        if (msg.sender == addowner) _;
        else
            emit ThrowError(
                "No tiene permisos para ejecutar la función / You does not have permissions to exectue this service"
            );
    }

    modifier onlyRecinto() {
        if (msg.sender == addowner) _;
        else
            emit ThrowError(
                "No tiene permisos para ejecutar la función / Yoo does not have permissions to exectue this service"
            );
    }

    //Registro de la Factura por el Owner.
    function RegistroFactura(
        address _paddReceptor,
        uint256 _pfactura,
        uint256 _pfecha,
        bytes32 _pRFC,
        bytes32 _ptipomaniobra,
        string memory _strCliente,
        string memory _strNomAgente,
        string memory _strUrl
    ) public returns (bytes32) {
        /*
         address addEmisor;
            address addReceptor;
            uint intFolioSIP;
            uint intfecha;
            bytes32 byRFC;   
            bytes32 byTrafico;
            string strCliente;
            string  strNomAgente;
            string  strUrl;
            */
        FacturasMuellaje[_pfactura].addEmisor = msg.sender;
        FacturasMuellaje[_pfactura].addReceptor = _paddReceptor;
        FacturasMuellaje[_pfactura].intFolioSIP = _pfactura;
        FacturasMuellaje[_pfactura].intfecha = _pfecha;
        FacturasMuellaje[_pfactura].byRFC = _pRFC;
        FacturasMuellaje[_pfactura].byTrafico = _ptipomaniobra;
        FacturasMuellaje[_pfactura].strCliente = _strCliente;
        FacturasMuellaje[_pfactura].strNomAgente = _strNomAgente;
        FacturasMuellaje[_pfactura].strUrl = _strUrl;
        emit EventRegistroFactura(
            msg.sender,
            _pfactura,
            _pfecha,
            _pRFC,
            _strUrl,
            _strCliente
        );

        return ("1");
    }

    //Consulta de la Factura por Recinto.
    function ConsultaFactura(uint256 _valor)
        public
        view
        returns (
            address,
            address,
            uint256,
            uint256,
            bytes32,
            bytes32,
            string memory,
            string memory,
            string memory
        )
    {
        EncabezadoFactura memory u = FacturasMuellaje[_valor];

        return (
            u.addEmisor,
            u.addReceptor,
            u.intFolioSIP,
            u.intfecha,
            u.byRFC,
            u.byTrafico,
            u.strCliente,
            u.strNomAgente,
            u.strUrl
        );
    }

    function EmiteEvento(
        address pasddress,
        uint256 pFolio,
        uint256 pFecha,
        string memory pdescripcion
    ) private {
        emit EventConsultaRecinto(pasddress, pFolio, pFecha, pdescripcion);
    }

    function ConsultaFactura2(uint256[] memory _valor)
        public
        pure
        returns (uint256, uint256)
    {
        return (_valor[0], _valor[1]);
        //return(1,2,3,4);
    }
}
