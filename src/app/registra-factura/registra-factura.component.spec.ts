import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistraFacturaComponent } from './registra-factura.component';

describe('RegistraFacturaComponent', () => {
  let component: RegistraFacturaComponent;
  let fixture: ComponentFixture<RegistraFacturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistraFacturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistraFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
