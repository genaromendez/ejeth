import { Component, AfterViewInit, ElementRef, ViewChild,ViewChildren,QueryList} from '@angular/core';
import { Web3Service } from '../services/web3.service';
import { Factura } from '../models/Factura';
@Component({
  selector: 'app-root',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements AfterViewInit {
  @ViewChild('scrollframe',{static:false}) private scrollFrame!: ElementRef;
  @ViewChildren('item') itemElements!: QueryList<any>;

  private scrollContainer: any;

  title = 'Ejemplo Ethereum';
   
  facturaConsultar = 0;
  fromBlock = 10900000;  

  factura: Factura = new Factura();

  blockHash = '';
  blockNumber = '';
  from = '';
  transactionHash = '';

  elementos: any = [];  

  cabeceras = ['Transaction Hash', 'Block Number','Solicitante', 'Factura', 'Fecha','RFC','Nombre','URL'];

  constructor(public web3s: Web3Service){}

  ngAfterViewInit(): void {
    this.conectar();
    //this.scrollContainer = this.scrollFrame.nativeElement;      
  }

  private onElementosChanged(): void {
    this.scrollToBottom();
  }

  conectar():void {
    if (!this.web3s.conectado) {
      this.web3s.connectAccount().then((r)=>{ 
        this.web3s.msgEstado = "Conectado.";
        this.web3s.conectado = true;
        this.subscribeToEvents();
      });
    }
  }

  setBlockFrom(newBlock:string):void {
    this.fromBlock = Number(newBlock);
    this.elementos = [];
    this.subscribeToEvents();
  }

  getConsultaFactura(): void {    
    this.factura.clear()
    this.web3s.contrato.methods.ConsultaFactura(this.facturaConsultar*1).call({from: this.web3s.accounts[0]}).then((response: any) => {                                                        
                                                        this.factura.emisor = response[0];
                                                        this.factura.receptor = response[1];
                                                        this.factura.folioSIP = response[2];
                                                        this.factura.fecha    = new Date(response[3]*1);
                                                        this.factura.rfc = this.web3s.web3js.utils.toAscii(response[4]);
                                                        this.factura.byTrafico = this.web3s.web3js.utils.toAscii(response[5]);
                                                        this.factura.strCliente = response[6];
                                                        this.factura.strNomAgente = response[7];
                                                        this.factura.strUrl = response[8];

                                                        console.log(this.factura.byTrafico);
                                                        
                                                        if(this.factura.folioSIP == 0) this.factura.folioSIP = -1;
                                                       });    
  }

  borrar(): void {
    this.blockHash = "";
    this.blockNumber = "";
    this.from = "";
    this.transactionHash = "";
  }  

  subscribeToEvents() {
    // Subscribe to pending transactions
    const self = this;
    this.web3s.contrato.events.EventRegistroFactura({
                                              fromBlock: this.fromBlock
                                            },
                                            (error: any, event: any) => {
                                              if (!error){                                                  
                                                  //setInterval(() => {
                                                    // Se multiplica * 1 para convertirlo a numero
                                                    const d = new Date(event.returnValues.fecha*1).toLocaleDateString("en-US");
                                                    this.elementos.push(
                                                      { blockHash: event.blockHash,                                                      
                                                        transactionHash: event.transactionHash,
                                                        blockNumber:event.blockNumber,                                             
                                                        solicitante: event.returnValues.solicitante,
                                                        nofactura: event.returnValues.nofactura,
                                                        idfactura: event.returnValues.idfactura,
                                                        fecha: new Date(event.returnValues.fecha*1).toLocaleDateString("es-MX"),
                                                        rfc: this.web3s.web3js.utils.toAscii(event.returnValues.rfc),
                                                        url: event.returnValues.url,
                                                        strMensaje: event.returnValues.strMensaje
                                                      }
                                                    );                                                                                                      
                                                // }, 500);                                                                                                
                                              }                                              
                                            });

  }

  scrollToBottom() {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }
}

